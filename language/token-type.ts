export class TokenType<T extends string = string> {
  public readonly pattern: RegExp | string
  constructor(public readonly name: T, pattern?: string | RegExp) {
    if (pattern == null) {
      pattern = name
    }
    this.pattern = pattern
  }
}
