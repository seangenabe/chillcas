import {
  ChillculatorNode,
  BinaryOperationNode,
  UnaryOperationNode,
  FunctionOrConstantNode,
  MinusOnlyNode,
  AdditiveNode,
  parseString
} from "./parser"
import { PLUS, TIMES } from "./lexer"
import { TokenType } from "./token-type"

export const PARTIAL: unique symbol = Symbol()

const constants = new Map<string, number>([["π", Math.PI], ["e", Math.E]])
const functions = new Map<string, (...args: number[]) => number>([
  ["sin", Math.sin],
  ["cos", Math.cos],
  ["tan", Math.tan]
])

export class Visitor {
  calculations = 0

  reset() {
    this.calculations = 0
  }

  visit(
    node: AdditiveNode | MinusOnlyNode | undefined
  ): number | typeof PARTIAL | undefined {
    if (node == null) {
      return undefined
    }
    return this.recursiveVisit(node)
  }

  private recursiveVisit(
    node: ChillculatorNode,
    partial?: boolean
  ): number | typeof PARTIAL {
    if (node.type === "Additive") {
      return this.binaryOperationCase(node, partial, (a, b, op) => {
        if (op === PLUS) {
          return a + b
        } else {
          return a - b
        }
      })
    } else if (node.type === "UnaryNegative") {
      const ret = this.unaryOperationCase(node, partial, a => -a, true)
      // Do not count calculations for negative numbers.
      if (node.operand && node.operand.type !== "NumericLiteral") {
        this.calculations++
      }
      return ret
    } else if (node.type === "Multiplicative") {
      return this.binaryOperationCase(node, partial, (a, b, op) => {
        if (op === TIMES) {
          return a * b
        } else {
          return a / b
        }
      })
    } else if (node.type === "Percent") {
      return this.unaryOperationCase(node, partial, a => a / 100)
    } else if (node.type === "Power") {
      return this.binaryOperationCase(node, partial, (a, b) => Math.pow(a, b))
    } else if (node.type === "Sqrt") {
      return this.unaryOperationCase(node, partial, a => Math.sqrt(a))
    } else if (node.type === "MinusOnly") {
      return 0
    } else if (node.type === "Group") {
      if (node.child != null) {
        return this.recursiveVisit(node.child)
      }
      return PARTIAL
    } else if (node.type === "FunctionOrConstant") {
      const isFunction = node.asFunction || node.args.length > 0
      // `foo(`, `foo(1)`, `foo(1, 2)`, or `foo(1,`
      if (isFunction) {
        return this.evaluateFunction(node, partial)
      }
      const c = constants.get(node.name.value)
      // `foo`
      if (c != null) {
        return c
      } else {
        // `interpreted as foo() without parenthesis`
        return this.evaluateFunction(node, partial)
      }
    } else {
      return node.value
    }
  }

  evaluateFunction(node: FunctionOrConstantNode, partial?: boolean) {
    const fn = functions.get(node.name.value)
    if (fn) {
      if (node.args.length < fn.length) {
        return PARTIAL
      } else if (node.args.length > fn.length) {
        throw new Error(
          `Invalid number of arguments for function: ${node.name.value}`
        )
      }
      const argValues = node.args.map(arg => this.recursiveVisit(arg, partial))
      if (argValues.some(v => v === PARTIAL)) {
        return PARTIAL
      }
      return fn(...(argValues as number[]))
    } else {
      throw new Error(`Invalid function: ${node.name.value}`)
    }
  }

  binaryOperationCase<Type extends string, TokenTypeName extends string>(
    node: BinaryOperationNode<Type, TokenTypeName, ChillculatorNode>,
    partial: boolean | undefined,
    op: (a: number, b: number, tokenType: TokenType<TokenTypeName>) => number
  ): number | typeof PARTIAL {
    if (partial && node.partial) {
      throw new Error("Nested partial expressions are not allowed.")
    }
    const operands = node.operands.map(n =>
      this.recursiveVisit(n, partial || node.partial)
    )
    this.calculations += node.operands.length - 1
    if (operands.length === 1) {
      return operands[0]
    }
    return operands.reduce((a, b, i) => {
      if (b === PARTIAL) return a
      return op(a as number, b, node.operators[i - 1].type)
    })
  }

  unaryOperationCase<Type extends string, TokenTypeName extends string>(
    node: UnaryOperationNode<Type, TokenTypeName, ChillculatorNode>,
    partial: boolean | undefined,
    op: (a: number) => number,
    noCount: boolean = false
  ): number | typeof PARTIAL {
    if (partial && node.partial) {
      throw new Error("Nested partial expressions are not allowed.")
    }
    if (!node.operand) {
      return PARTIAL
    }
    if (node.operator) {
      const r = this.recursiveVisit(node.operand, partial || node.partial)
      if (r === PARTIAL) {
        return r
      }
      if (!noCount) {
        this.calculations += 1
      }
      return op(r)
    } else {
      return this.recursiveVisit(node.operand, partial || node.partial)
    }
  }
}

const staticVisitor = new Visitor()

export function visitAst(ast: AdditiveNode | MinusOnlyNode | undefined) {
  staticVisitor.reset()
  return {
    result: staticVisitor.visit(ast),
    calculations: staticVisitor.calculations
  } as const
}

export function visitString(input: string) {
  return visitAst(parseString(input))
}
