import {
  Token,
  MINUS,
  PLUS,
  NumericLiteral,
  POWER,
  SQRT,
  TIMES,
  DIVIDE,
  END,
  LPAREN,
  RPAREN,
  Alphanumeric,
  COMMA,
  TokenOfType,
  GetTokenName,
  lex,
  PERCENT
} from "./lexer"
import { TokenType } from "./token-type"
/** Sentinel token for the end of the string */
const EOS: Token<"END"> = { type: END, value: "" }

export class Parser {
  tokens: Token[] = []

  parseNumericLiteral(): NumericLiteralNode | undefined {
    const nextTokenType = this.next.type
    if (nextTokenType === NumericLiteral) {
      const str = this.chomp().value
      let value: number
      if (str === ".") {
        value = 0
      } else {
        value = Number.parseFloat(str.replace("−", "-"))
      }
      return { type: "NumericLiteral", value }
    }
    return undefined
  }

  parseGroupExpression(): GroupNode | undefined {
    const lParen = this.chompIfType(LPAREN)
    if (lParen == null) {
      return undefined
    }
    const misplacedRParen = this.chompIfType(RPAREN)
    if (misplacedRParen != null) {
      throw new Error(
        "Unexpected closing parenthesis right after open parenthesis."
      )
    }
    const child = this.parseAdditiveExpression()
    this.chompIfType(RPAREN)
    return { type: "Group", child }
  }

  parseFunctionOrConstantExpression(): FunctionOrConstantNode | undefined {
    const name = this.chompIfType(Alphanumeric)
    if (name == null) {
      return undefined
    }
    const lParen = this.chompIfType(LPAREN)
    if (lParen) {
      const args = this.parseArgumentList()
      this.chompIfType(RPAREN)
      return { type: "FunctionOrConstant", name, args, asFunction: true }
    } else {
      const arg = this.parseAdditiveExpression()
      const args = arg == null ? [] : [arg]
      return { type: "FunctionOrConstant", name, args }
    }
  }

  parseArgumentList() {
    const args: AdditiveNode[] = []
    while (!this.isEmpty) {
      const r = this.parseAdditiveExpression()
      if (r) {
        args.push(r)
      } else {
        break
      }
      if (this.chompIfType(COMMA) == null) {
        break
      }
    }
    return args
  }

  parseUnaryNegativeExpression(): UnaryNegativeNode | undefined {
    return this.prefixOperation({
      type: "UnaryNegative",
      operatorTokenTypes: [MINUS] as const,
      descend: () => this.parseAtomicExpression()
    })
  }

  parseAtomicExpression(): AtomicNode | undefined {
    return (
      this.parseNumericLiteral() ||
      this.parseGroupExpression() ||
      this.parseFunctionOrConstantExpression()
    )
  }

  parsePowerExpression(): PowerNode | undefined {
    return this.infixOperation({
      type: "Power",
      operatorTokenTypes: [POWER] as const,
      descend: () => this.parseUnaryNegativeExpression()
    })
  }

  parseSqrtExpression(): SqrtNode | undefined {
    return this.prefixOperation({
      type: "Sqrt",
      operatorTokenTypes: [SQRT] as const,
      descend: () => this.parsePowerExpression()
    })
  }

  parsePercentExpression(): PercentNode | undefined {
    return this.suffixOperation({
      type: "Percent",
      operatorTokenTypes: [PERCENT] as const,
      descend: () => this.parseSqrtExpression()
    }) as PercentNode | undefined
  }

  parseMultiplicativeExpression(): MultiplicativeNode | undefined {
    return this.infixOperation({
      type: "Multiplicative",
      operatorTokenTypes: [TIMES, DIVIDE] as const,
      descend: () => this.parsePercentExpression(),
      multiplyConsecutiveOperands: true
    })
  }

  parseAdditiveExpression(): AdditiveNode | undefined {
    return this.infixOperation({
      type: "Additive",
      operatorTokenTypes: [PLUS, MINUS] as const,
      descend: () => this.parseMultiplicativeExpression()
    })
  }

  /**
   * Top-level parse. Parses the input token array.
   * @returns {undefined | MinusOnlyNode | AdditiveNode}
   * `undefined` if the input token array is empty.
   * A `MinusOnly` node if the input only has a `MINUS` token.
   * An `AdditiveNode` if the input has been parsed successfully.
   * @throws {Error} The input has not been parsed successfully.
   */
  parse() {
    if (this.tokens.length === 1 && this.tokens[0].type === MINUS) {
      return { type: "MinusOnly" } as MinusOnlyNode
    }
    const result = this.parseAdditiveExpression()
    // Parse top-level minus sign.
    if (!this.isEmpty) {
      throw new Error(
        `Unexpected token: ${this.next.value} with type: ${this.next.type.name}`
      )
    }
    return result
  }

  /**
   * Peek at the next token in the queue.
   * @param tokens
   */
  private get next(): Token {
    if (this.isEmpty) {
      return EOS
    }
    return this.tokens[0]
  }

  /**
   * Chomp the next token in the queue.
   * @param tokens
   */
  private chomp(): Token<string> {
    if (this.isEmpty) {
      return EOS
    }
    return this.tokens.shift()!
  }

  private chompIfType<TokenTypeName extends string>(
    ...types: TokenType<TokenTypeName>[]
  ): Token<TokenTypeName> | undefined {
    if ((types as TokenType<string>[]).includes(this.next.type)) {
      return this.chomp() as Token<TokenTypeName>
    }
    return undefined
  }

  private get isEmpty() {
    return this.tokens.length === 0
  }

  /**
   * Helper function for a unary operation
   * with a prefix operator.
   */
  prefixOperation<Type extends string, TokenTypeName extends string, Operand>({
    type,
    operatorTokenTypes,
    descend
  }: {
    type: Type
    operatorTokenTypes: Iterable<TokenType<TokenTypeName>>
    descend: () => Operand | undefined
  }): UnaryOperationNode<Type, TokenTypeName, Operand> | undefined {
    if (this.isEmpty) {
      return undefined
    }
    // Read an operator.
    const operator = this.chompIfType(...operatorTokenTypes)
    if (operator == null) {
      // Alternate to descend only. (Do not perform operation)
      const operand = descend()
      if (operand != null) {
        return { type, operand }
      } else {
        return undefined
      }
    }
    if (this.isEmpty) {
      // Partial.
      return { type, operator, partial: true }
    }
    // Read an operand.
    const operand = descend()
    if (operand != null) {
      return { type, operator, operand }
    } else {
      return undefined
    }
  }

  suffixOperation<Type extends string, TokenTypeName extends string, Operand>({
    type,
    operatorTokenTypes,
    descend
  }: {
    type: Type
    operatorTokenTypes: Iterable<TokenType<TokenTypeName>>
    descend: () => Operand | undefined
  }): UnaryOperationNode<Type, TokenTypeName, Operand> | undefined {
    if (this.isEmpty) {
      return undefined
    }
    // Read an operand.
    const operand = descend()
    if (operand == null) {
      return undefined
    }
    // Read an operator.
    const operator = this.chompIfType(...operatorTokenTypes)
    return { type, operand, operator }
  }

  /**
   * Helper function for a binary operation
   * with an infix operator.
   */
  infixOperation<Type extends string, TokenTypeName extends string, Operand>({
    type,
    operatorTokenTypes,
    descend,
    multiplyConsecutiveOperands
  }: {
    type: Type
    operatorTokenTypes: Iterable<TokenType<TokenTypeName>>
    descend: () => Operand | undefined
    multiplyConsecutiveOperands?: boolean
  }): BinaryOperationNode<Type, TokenTypeName, Operand> | undefined {
    if (this.isEmpty) {
      return undefined
    }
    const operands: Operand[] = []
    const operators: Token<TokenTypeName>[] = []
    while (!this.isEmpty) {
      // Read an operand.
      const operand = descend()
      // Check if successful parse.
      if (operand == null) {
        break
      }
      operands.push(operand)

      // Read an operator.
      if (this.isEmpty) {
        break
      }
      const operator = this.chompIfType(...operatorTokenTypes)
      if (operator != null) {
        operators.push(operator)
        continue
      }
      if (!multiplyConsecutiveOperands) {
        break
      }
      if (this.next.type === MINUS) {
        // Expecting additive operation, do not accept.
        break
      }
      operators.push({ type: TIMES, value: "" } as Token<TokenTypeName>)
    }
    if (operands.length === 0) {
      return undefined
    }
    return {
      type,
      operands,
      operators,
      partial: operands.length === operators.filter(op => op.value).length
    }
  }
}

const staticParser = new Parser()
export function parseTokens(tokens: Token[]) {
  staticParser.tokens = tokens
  return staticParser.parse()
}

export function parseString(input: string) {
  return parseTokens(lex(input))
}

export interface FunctionOrConstantNode {
  type: "FunctionOrConstant"
  name: TokenOfType<typeof Alphanumeric>
  args: AdditiveNode[]
  asFunction?: true
}

export interface GroupNode {
  type: "Group"
  child?: AdditiveNode
}

export interface MinusOnlyNode {
  type: "MinusOnly"
}

export interface NumericLiteralNode {
  type: "NumericLiteral"
  value: number
}

export type AtomicNode = NumericLiteralNode | GroupNode | FunctionOrConstantNode

export interface UnaryNegativeNode
  extends UnaryOperationNode<
    "UnaryNegative",
    GetTokenName<typeof MINUS>,
    AtomicNode
  > {}

export interface PowerNode
  extends BinaryOperationNode<
    "Power",
    GetTokenName<typeof POWER>,
    UnaryNegativeNode
  > {}

export interface SqrtNode
  extends UnaryOperationNode<"Sqrt", GetTokenName<typeof SQRT>, PowerNode> {}

export interface PercentNode
  extends UnaryOperationNode<
    "Percent",
    GetTokenName<typeof PERCENT>,
    SqrtNode
  > {
  // Suffix operators cannot be partial.
  partial?: never
}

export interface MultiplicativeNode
  extends BinaryOperationNode<
    "Multiplicative",
    GetTokenName<typeof TIMES | typeof DIVIDE>,
    PercentNode
  > {}

export interface AdditiveNode
  extends BinaryOperationNode<
    "Additive",
    GetTokenName<typeof PLUS | typeof MINUS>,
    MultiplicativeNode
  > {}

export type ChillculatorNode =
  | AdditiveNode
  | MultiplicativeNode
  | PercentNode
  | SqrtNode
  | PowerNode
  | NumericLiteralNode
  | MinusOnlyNode
  | GroupNode
  | UnaryNegativeNode
  | FunctionOrConstantNode

export interface UnaryOperationNode<
  Type extends string,
  TokenTypeName extends string,
  Operand
> {
  type: Type
  /** undefined if alternated to pass-through (no operations done) */
  operator?: Token<TokenTypeName>
  /** undefined if partial */
  operand?: Operand
  partial?: true
}

export interface BinaryOperationNode<
  Type extends string,
  TokenTypeName extends string,
  Operand
> {
  type: Type
  operands: Operand[]
  operators: Token<TokenTypeName>[]
  partial?: boolean
}
