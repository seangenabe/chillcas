"use strict"

import { TokenType } from "./token-type"

export const PLUS = new TokenType("+")
export const MINUS = new TokenType("−") // minus, not hyphen-minus
export const TIMES = new TokenType("×")
export const DIVIDE = new TokenType("÷")
export const SQRT = new TokenType("√")
export const LPAREN = new TokenType("(")
export const RPAREN = new TokenType(")")
export const POWER = new TokenType("^")
export const COMMA = new TokenType(",")
export const PERCENT = new TokenType("%")
export const NumericLiteral = new TokenType(
  "NumericLiteral",
  /^([0-9]*\.[0-9]*(E[+−]?[0-9]*)?|[0-9]+(E[+−]?[0-9]*)?)/
)
export const Alphanumeric = new TokenType(
  "Alphanumeric",
  /^([A-Za-z][A-Za-z0-9]*|π)/
)
/**
 * Placeholder token type for the end of the string.
 */
export const END = new TokenType("END")
/**
 * Sentinel token type.
 */
const NONE = new TokenType("NONE")

export const tokenTypes = [
  PLUS,
  MINUS,
  TIMES,
  DIVIDE,
  PERCENT,
  SQRT,
  LPAREN,
  RPAREN,
  POWER,
  COMMA,
  NumericLiteral,
  Alphanumeric
] as const
export type TokenName =
  | GetTokenName<typeof PLUS>
  | GetTokenName<typeof MINUS>
  | GetTokenName<typeof TIMES>
  | GetTokenName<typeof DIVIDE>
  | GetTokenName<typeof PERCENT>
  | GetTokenName<typeof SQRT>
  | GetTokenName<typeof LPAREN>
  | GetTokenName<typeof RPAREN>
  | GetTokenName<typeof POWER>
  | GetTokenName<typeof COMMA>
  | GetTokenName<typeof NumericLiteral>
  | GetTokenName<typeof Alphanumeric>

export class Lexer {
  lastTokenType = NONE
  input = ""

  reset() {
    this.lastTokenType = NONE
    this.input = ""
  }

  /**
   * Lexes the input string into a stream of tokens.
   */
  *lex(): IterableIterator<Token<TokenName>> {
    outerWhile: while (this.input.length !== 0) {
      for (let tokenType of tokenTypes) {
        const { pattern } = tokenType
        if (typeof pattern === "string") {
          // Match exactly `pattern` string.
          if (this.input.startsWith(pattern)) {
            yield { type: tokenType, value: pattern }
            this.input = this.input.substring(pattern.length)
            continue outerWhile
          }
        } else {
          // Match the string with `pattern` regex.
          const regexResult = pattern.exec(this.input)
          if (regexResult != null) {
            yield {
              type: tokenType,
              value: this.input.substring(0, regexResult[0].length)
            }
            this.input = this.input.substring(regexResult[0].length)
            continue outerWhile
          }
        }
      }
      throw new Error(`Unable to lex string: ${this.input}`)
    }
  }
}

const staticLexer = new Lexer()
export function lex(input: string): Token<TokenName>[] {
  staticLexer.reset()
  staticLexer.input = input
  return [...staticLexer.lex()]
}

export interface Token<T extends string = string> {
  type: TokenType<T>
  value: string
}

export type GetTokenName<TT> = TT extends TokenType<infer N> ? N : never

export type TokenOfType<TT> = Token<GetTokenName<TT>>
