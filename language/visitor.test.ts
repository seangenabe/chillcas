import test, { ExecutionContext, TestInterface } from "ava"
import { Parser } from "./parser"
import { Lexer, Token } from "./lexer"
import { Visitor, PARTIAL } from "./visitor"

const tests: {
  name: string
  expr: string
  result: number | typeof PARTIAL
}[] = [
  {
    name: "integer",
    expr: "123",
    result: 123
  },
  { name: "decimal number", expr: "1.1", result: 1.1 },
  {
    name: "addition",
    expr: "1+2",
    result: 3
  },
  {
    name: "subtraction",
    expr: "3−2",
    result: 1
  },
  {
    name: "multiplication",
    expr: "3×2",
    result: 6
  },
  {
    name: "division",
    expr: "6÷2",
    result: 3
  },
  {
    name: "negative number",
    expr: "−1",
    result: -1
  },
  {
    name: "addition with negative number",
    expr: "3+−1",
    result: 2
  },
  {
    name: "subtraction with negaitve number",
    expr: "3−−1",
    result: 4
  },
  {
    name: "multiplication with negative number",
    expr: "3×−2",
    result: -6
  },
  {
    name: "division with negative number",
    expr: "6÷−2",
    result: -3
  },
  {
    name: "multiplication over addition",
    expr: "2+3×4",
    result: 14
  },
  {
    name: "grouping",
    expr: "(2+3)×6",
    result: 30
  },
  {
    name: "incomplete expression without a result",
    expr: "2+",
    result: 2
  },
  {
    name: "negative incomplete decimal",
    expr: "−1.1×",
    result: -1.1
  },
  {
    name: "partial expression",
    expr: "1+2+",
    result: 3
  },
  {
    name: "negative expression",
    expr: "−(−1.7)",
    result: 1.7
  },
  {
    name: "minus sign only",
    expr: "−",
    result: 0
  },
  {
    name: "function with single argument",
    expr: "sin(0)",
    result: 0
  },
  {
    name: "function name + paren",
    expr: "sin(",
    result: PARTIAL
  },
  {
    name: "function with single argument without parentheses",
    expr: "sin√0",
    result: 0
  },
  {
    name: "constant",
    expr: "π",
    result: Math.PI
  },
  {
    name: "negative constant",
    expr: "−π",
    result: -Math.PI
  },
  {
    name: "percent",
    expr: "50%",
    result: 0.5
  }
]

const lexer = new Lexer()
const parser = new Parser()
const visitor = new Visitor()

for (let { name, expr, result } of tests) {
  const testBody = (t: ExecutionContext) => {
    const tokens = lex(expr)
    const ast = parse(tokens)
    if (ast == null) {
      t.fail("failed to parse")
      return
    }
    visitor.reset()
    const actual = visitor.visit(ast)
    t.is(actual, result)
  }
  test(`eval ${name}`, testBody)
}

function lex(input: string) {
  lexer.reset()
  lexer.input = input
  return [...lexer.lex()]
}

function parse(tokens: Token[]) {
  parser.tokens = tokens
  return parser.parse()
}
