import test from "ava"
import {
  PLUS,
  DIVIDE,
  MINUS,
  TIMES,
  SQRT,
  LPAREN,
  RPAREN,
  POWER,
  lex,
  Alphanumeric
} from "./lexer"
import { single } from "ix/iterable/single"

for (let significand of [".", "123", ".123", "123.456"]) {
  for (let exponent of ["", "E", "E78", "E−789", "E+789"]) {
    const number = significand + exponent
    test(`lex number ${number}`, t => {
      const tokens = lex(number)
      if (tokens.length > 1) {
        t.log(tokens)
        t.fail("More than one token found.")
        return
      }
      t.is(tokens[0].value, number)
    })
  }
}

test("lex symbols", t => {
  t.is(lexSingle("+").type, PLUS)
  t.is(lexSingle("−").type, MINUS)
  t.is(lexSingle("×").type, TIMES)
  t.is(lexSingle("÷").type, DIVIDE)
  t.is(lexSingle("√").type, SQRT)
  t.is(lexSingle("(").type, LPAREN)
  t.is(lexSingle(")").type, RPAREN)
  t.is(lexSingle("^").type, POWER)
})

test("lex constants", t => {
  t.is(lexSingle("π").type, Alphanumeric)
  t.is(lexSingle("e").type, Alphanumeric)
})

function lexSingle(str: string) {
  const token = single(lex(str))
  if (!token) {
    throw new Error(`token ${str} not lexed`)
  }
  return token
}
